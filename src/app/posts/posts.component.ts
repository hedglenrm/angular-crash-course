import { Component, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';


@Component({
     selector: 'posts',
     templateUrl: './posts.component.html',
     styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
     posts: any[];


     constructor(private postsService: PostsService) {

     }

     ngOnInit() {
          this.postsService.getPosts()
               .subscribe(response => {
                    this.posts = response.json();
               }, error => {
                    alert('An unexpected error occurred');
                    console.log('An unexpected error occurred');
               });
     }

     createPost(input: HTMLInputElement) {
          let post = {
               title: input.value
          };
          input.value = '';
          this.postsService.createPost(post)
               .subscribe(
                    response => {
                         post['id'] = response.json().id;
                         this.posts.splice(0, 0, post);
                         console.log(response.json());
                    },
                    (error: Response) => {
                         if(error.status === 400){
                              // this.form.setErrors(error.json());
                              
                         }
                         alert('An unexpected error occurred');
                         console.log('An unexpected error occurred');
                    }
               );
     }

     updatePost(post) {
          this.postsService.updatePost(post.id)
               .subscribe(
                    response => {
                         console.log(response.json());
                    },
                    error => {
                         alert('An unexpected error occurred');
                         console.log('An unexpected error occurred');
                    }
               );
     }

     deletePost(post) {
          this.postsService.deletePost(345)
               .subscribe(
                    response => {
                         let index = this.posts.indexOf(post);
                         this.posts.splice(index, 1);

                    },
                    (error: Response) => {
                         if(error.status === 404){
                              alert('This post has already been deleted');
                         }else{
                              alert('An unexpected error occurred');
                              console.log('An unexpected error occurred');

                         }
                    }
               );
     }

}
