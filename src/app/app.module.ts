import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { NewCourseFormComponent } from './new-course-form/new-course-form.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './services/posts.service';



@NgModule({
     declarations: [
          AppComponent,
          ContactFormComponent,
          SignupFormComponent,
          NewCourseFormComponent,
          ChangePasswordComponent,
          PostsComponent
     ],
     imports: [
          BrowserModule,
          AppRoutingModule,
          FormsModule,
          ReactiveFormsModule,
          HttpModule
     ],
     providers: [
          PostsService
     ],
     bootstrap: [AppComponent]
})
export class AppModule { }
