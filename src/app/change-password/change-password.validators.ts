import { ValidationErrors, AbstractControl } from '@angular/forms';


export class ChangePasswordValidators{

     oldPassword = '1234';
     newPassword: string;

     static verifyOldPassword(control: AbstractControl): Promise<ValidationErrors | null> {
          
          return new Promise((resolve) => {
               setTimeout(() => {
                    if(control.value !== '1234'){
                         resolve({ invalidOldPassword: true});
                    }
                    resolve(null);
               },2000);
          });
               
     }

     static verifyNewPassword(control: AbstractControl): ValidationErrors | null {

          let newPassword = control.get('newpassword');
          let verifyPassword = control.get('verifypassword');

          if(newPassword.value !== verifyPassword.value){
               return { passwordDoesntMatch: true }
          }
          return null;
          
     }


}