import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ChangePasswordValidators } from './change-password.validators';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  form;

  
  constructor() {

    this.form = new FormGroup({
      oldpassword: new FormControl('', [Validators.required], [ChangePasswordValidators.verifyOldPassword]),
      newpassword: new FormControl('', [Validators.required]),
      verifypassword: new FormControl('', [Validators.required,])
    },[
      ChangePasswordValidators.verifyNewPassword
    ])
  }

  get oldpassword() {
    return this.form.get('oldpassword');
  }

  get newpassword() {
    return this.form.get('newpassword');
  }

  get verifypassword() {
    return this.form.get('verifypassword');
  }

  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }


}
